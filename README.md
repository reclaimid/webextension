![re:claimID](https://gitlab.com/reclaimid/ui/raw/master/src/assets/reclaim_icon.png)

# re:claimID webextension

This is the source of the re:claimID webextension.

## Installation

The easiest way to install this extension is through the respective store.

* [Mozilla Firefox](https://addons.mozilla.org/firefox/addon/reclaimid)
* Google Chrome / Chromium (WIP)


## Build
To build this addon, simply execute `make`.
The resulting file `reclaim-plugin.zip` contains the built webextension for use either with Mozilla Firefox or Chrome.

## See also
The code of the user interface is found separately in the [UI repository](https://gitlab.com/reclaimid/ui) which is included as a submodule.
